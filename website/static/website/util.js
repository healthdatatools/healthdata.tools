export const saveOrUpdateSettings = async ({
  presetId,
  presetName,
  seriesColor,
  lineWeight,
  lineType,
  startValue,
  endValue,
}) => {
  const csrfToken = getCookie("csrftoken");
  if (!presetName) {
    alert("Preset name is required.");
    return;
  }
  const settings = {
    name: presetName.trim(),
    series_color: seriesColor,
    line_weight: lineWeight,
    line_type: lineType,
    start_value: startValue,
    end_value: endValue,
  };
  try {
    let url = presetId
      ? `/update_chart_settings/${presetId}/`
      : "/save_chart_settings/";
    let method = presetId ? "PUT" : "POST";

    const response = await fetch(url, {
      method: method,
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": csrfToken,
      },
      body: JSON.stringify(settings),
    });

    const data = await response.json();
    if (response.ok) {
      alert("Settings saved successfully!");
      if (!presetId) return data.id;
    } else {
      console.error("Error saving settings:", data.errors);
    }
  } catch (error) {
    console.error("Network error:", error);
  }
};

const getCookie = (name) => {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      if (cookie.startsWith(name + "=")) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
};
