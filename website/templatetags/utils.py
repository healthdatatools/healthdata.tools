import datetime
import subprocess

from pathlib import Path
from django import template
from django.forms import Widget

from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent


register = template.Library()


@register.simple_tag
def get_domain_version():
    versionfile = BASE_DIR / "domain_version.txt"
    if versionfile.is_file():
        return versionfile.read_text()
    else:
        result = subprocess.run(
            ["git", "describe", "--long", "--abbrev=12", "--tags"],
            stdout=subprocess.PIPE,
        )
        result.check_returncode()
        return result.stdout.decode("utf-8").strip()


@register.simple_tag
def get_deployment_time():
    versionfile = BASE_DIR / "domain_version.txt"
    if versionfile.is_file():
        mtime = versionfile.stat().st_mtime
        dt = datetime.datetime.fromtimestamp(mtime)
        return str(dt)
    else:
        return str(datetime.datetime.now())


@register.filter
def format_form(form):
    html = []

    for field in form:
        if isinstance(field.field.widget, Widget):
            current_classes = field.field.widget.attrs.get("class", "")
            field.field.widget.attrs["class"] = f"{current_classes} form-input".strip()

            html.append(conditional_escape(field.errors))

            if field.label:
                html.append(
                    conditional_escape(field.label_tag(attrs={"class": "form-label"}))
                )

            html.append(conditional_escape(field))

    return mark_safe("\n".join(html))
