import datetime
import re
import tempfile
import unittest

from pathlib import Path
from unittest.mock import patch

import website.templatetags.utils as utils


# Create your tests here.
# Ref: https://docs.djangoproject.com/en/5.0/topics/testing/overview/
class TestUtils(unittest.TestCase):
    def __init__(self, foo):
        super().__init__(foo)
        self.TMP_DIR = None
        self.TMP_PATH = None
        self.DOMAIN_VERSION = "0.1.0-1234-gabcdef012345"
        self.DEPLOY_TIME = None

    def setUp(self):
        self.TMP_DIR = tempfile.TemporaryDirectory()
        self.TMP_PATH = Path(self.TMP_DIR.name).resolve()
        self.DV_PATH = self.TMP_PATH / "domain_version.txt"
        with open(self.DV_PATH, "w") as f:
            f.write(self.DOMAIN_VERSION)
        # read the deploy time
        self.DEPLOY_TIME = str(
            datetime.datetime.fromtimestamp(self.DV_PATH.stat().st_mtime)
        )

    def tearDown(self):
        self.TMP_DIR.cleanup()
        self.TMP_DIR = None
        self.TMP_PATH = None
        self.DV_PATH = None
        self.DEPLOY_TIME = None

    def testGetDomainVersionInDev(self):
        version = utils.get_domain_version()
        # version in a working directory should match something like: 0.1.0-2-g1b23df3aee66
        self.assertTrue(re.match("^.*-\\d+-g[a-f0-9]{12}$", version))

    def testGetDomainVersionInProd(self):
        with patch("website.templatetags.utils.BASE_DIR", self.TMP_PATH):
            # patch so we get a base_dir that contains a domain_version.txt with a known value, and ensure we get it
            version = utils.get_domain_version()
            self.assertEqual(version, self.DOMAIN_VERSION)

    def testGetDeploymentTimeInDev(self):
        now = datetime.datetime.now()
        # patch time so we know in advance what the time will be and can validate it
        # in dev when there is no deploy file, we should just return "now"
        with patch("website.templatetags.utils.datetime") as mock_dt:
            mock_dt.datetime.now.return_value = now
            deployment_time = utils.get_deployment_time()

            self.assertEqual(deployment_time, str(now))

    def testGetDeploymentTimeInProd(self):
        with patch("website.templatetags.utils.BASE_DIR", self.TMP_PATH):
            deployment_time = utils.get_deployment_time()
            self.assertEqual(deployment_time, self.DEPLOY_TIME)
