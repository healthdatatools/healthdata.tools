from django.shortcuts import render
from django.contrib.auth.decorators import login_required


def index(request):
    context = {}
    return render(request, "website/home.html", context)


def about(request):
    context = {}
    return render(request, "website/about.html", context)


@login_required
def profile(request):
    context = {}
    return render(request, "website/profile.html", context)
