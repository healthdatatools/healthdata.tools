import csv
from django import forms
from django.core import validators
from django.core.exceptions import ValidationError

from .models import DataSource, DataImport, DataImportCSVFormat


class ImportDataUploadForm(forms.Form):
    # data_source = forms.ModelChoiceField(queryset=None)

    file = forms.FileField(
        validators=[validators.FileExtensionValidator(allowed_extensions=".csv")]
    )

    # def __init__(self, *args, **kwargs):
    #     request = kwargs.pop('request', None)
    #     super().__init__(*args, **kwargs)
    #     if request:
    #         user = request.user
    #         self.fields['listdata'].queryset = DataSource.objects.filter(author=user)


class DataSourceForm(forms.ModelForm):
    class Meta:
        model = DataSource
        fields = ["name"]


class DataImportForm(forms.ModelForm):
    file = forms.FileField(widget=forms.FileInput(attrs={"accept": "text/csv"}))

    class Meta:
        model = DataImport
        fields = ["file"]

    def clean_file(self):
        file = self.cleaned_data["file"]

        if not file.name.lower().endswith(".csv"):
            raise ValidationError("Please upload a .csv file.")

        if file.size > 256 * 1024 * 1024:
            raise ValidationError("Maximum file size is 256 MB.")

        return file


class DataImportCSVFormatForm(forms.ModelForm):
    dialect = forms.ChoiceField(
        choices={dialect: dialect for dialect in csv.list_dialects()}
    )

    class Meta:
        model = DataImportCSVFormat
        fields = ["dialect", "skip_rows"]


class DataImportCSVColumnForm(forms.Form):
    enabled = forms.BooleanField()
    data_format = forms.CharField(required=False)


DataImportCSVColumnFormSet = forms.formset_factory(DataImportCSVColumnForm)
