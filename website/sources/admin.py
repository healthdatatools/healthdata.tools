from django.contrib import admin

from .models import DataSource, DataSourceColumnNumeric, DataImport, DataPointNumeric


class DataSourceAdmin(admin.ModelAdmin):
    list_display = ("user", "name", "created_at")


admin.site.register(DataSource, DataSourceAdmin)


class DataImportAdmin(admin.ModelAdmin):
    list_display = ("user", "data_source", "file_name", "created_at")


admin.site.register(DataImport, DataImportAdmin)


admin.site.register(DataSourceColumnNumeric)
admin.site.register(DataPointNumeric)
