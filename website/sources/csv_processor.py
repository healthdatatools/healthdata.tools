import csv
import logging
from datetime import datetime
from dataclasses import dataclass
from collections import Counter
from typing import Callable

from .models import DataImportCSVFormat
from django.db.models import FileField


@dataclass
class CSVType:
    key: str
    display_name: str
    parse: Callable[[str], any]


def parse_number(value):
    return float(value)


def make_parse_timestamp(format):
    def parse(value):
        return datetime.strptime(value, format)

    return parse


# Types are in order of preference
TYPE_PARSERS = [
    CSVType(
        "number",
        "Number",
        parse_number,
    ),
    CSVType(
        "timestamp|MM-DD-YYYY hh:mm AM",
        "Timestamp (MM-DD-YYYY hh:mm AM)",
        make_parse_timestamp(r"%m-%d-%Y %I:%M %p"),
    ),
    CSVType(
        "timestamp|YYYY-MM-DD HH:MM:SS",
        "Timestamp (YYYY-MM-DD HH:MM:SS)",
        make_parse_timestamp(r"%Y-%m-%d %H:%M:%S"),
    ),
    CSVType(
        "timestamp|MM/DD/YYYY HH:MM",
        "Timestamp (MM/DD/YYYY HH:MM)",
        make_parse_timestamp(r"%m/%d/%Y %H:%M"),
    ),
]

TYPE_PARSER_MAP = {csv_type.key: csv_type for csv_type in TYPE_PARSERS}


def _iter_file(field: FileField, format: DataImportCSVFormat):
    file = field.open("rt")
    lines = iter(file)

    if format.skip_rows:
        for i in range(format.skip_rows):
            next(lines)

    reader = csv.reader(lines, dialect=format.dialect)
    return reader


def get_info(field: FileField, format: DataImportCSVFormat):
    errors = []
    warnings = []
    column_names = []
    column_meta = []
    row_count = 0

    try:
        reader = _iter_file(field, format)

        column_names = next(reader)
        column_count = len(column_names)

        for row in reader:
            row_count += 1
            column_count = max(column_count, len(row))

            while len(column_meta) < len(row):
                column_meta.append(
                    {"has_data": False, "valid_types": list(TYPE_PARSER_MAP.keys())}
                )

            for meta, cell in zip(column_meta, row):
                if cell is None or cell.strip() == "":
                    continue

                meta["has_data"] = True

                for type in list(meta["valid_types"]):
                    try:
                        TYPE_PARSER_MAP[type].parse(cell)
                    except Exception:
                        meta["valid_types"].remove(type)

        if len(column_names) < column_count:
            warnings.append(
                f"{column_count - len(column_names)} columns did not have labels"
            )
            for i in range(len(column_names), column_count):
                column_names.append(f"column_{i + 1}")

        if column_count == 0:
            errors.append("File has no columns")
        elif row_count == 0:
            errors.append("File has no rows")

        for name, count in Counter(column_names).items():
            if count > 1:
                warnings.append(f'Column "{name}" appears {count} times')
    except StopIteration:
        errors.append("File did not have enough rows (check skip rows option)")
    except Exception as e:
        logging.exception("Error processing CSV file")
        errors.append(f"Error processing file: {e}")

    columns = [
        {
            "index": i,
            "name": name,
            "has_data": meta["has_data"],
            "valid_types": [
                type
                for key, type in TYPE_PARSER_MAP.items()
                if key in meta["valid_types"]
            ],
            "invalid_types": [
                type
                for key, type in TYPE_PARSER_MAP.items()
                if key not in meta["valid_types"]
            ],
        }
        for i, (name, meta) in enumerate(zip(column_names, column_meta))
    ]

    return {
        "errors": errors,
        "warnings": warnings,
        "columns": columns,
        "row_count": row_count,
    }


def iter_rows(field: FileField, format: DataImportCSVFormat, info, column_formats):
    reader = _iter_file(field, format)
    next(reader)  # Skip columns

    col_ty = [
        (col_info["name"], column_formats.get(col_info["name"])) for col_info in info
    ]

    for row in reader:
        yield {
            name: (
                None
                if cell is None or cell.strip() == ""
                else TYPE_PARSER_MAP[ty].parse(cell)
            )
            for (name, ty), cell in zip(col_ty, row)
            if ty is not None
        }
