from itertools import batched
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.text import slugify
from django.db import transaction
from django.db.models import Count
from django.db.models.functions import Now

from .forms import (
    DataImportCSVColumnFormSet,
    DataImportCSVFormatForm,
    DataSourceForm,
    DataImportForm,
)
from .models import (
    DataPointNumeric,
    DataSource,
    DataImport,
    DataImportCSVFormat,
    DataSourceColumnNumeric,
)
from . import csv_processor


@login_required
def sources_list(request):
    sources = (
        DataSource.objects.order_by("created_at")
        .filter(user=request.user)
        .annotate(dataimport_count=Count("dataimport"))
    )

    return render(request, "sources/list.html", {"sources": sources})


@login_required
def sources_view(request, slug: str, id: int):
    try:
        source = DataSource.objects.prefetch_related("dataimport_set").get(
            pk=id, user=request.user
        )
    except DataSource.DoesNotExist:
        return HttpResponseRedirect(reverse("website/sources"))

    actual_slug = slugify(source.name)
    if slug != actual_slug:
        return HttpResponseRedirect(
            reverse("website/sources/view", kwargs={"slug": actual_slug, "id": id})
        )

    imports = source.dataimport_set.all()

    return render(request, "sources/view.html", {"source": source, "imports": imports})


@login_required
def sources_create(request):
    if request.method == "POST":
        form = DataSourceForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.user = request.user
            model.save()
            return HttpResponseRedirect(
                reverse(
                    "website/sources/view",
                    kwargs={"slug": model.slug(), "id": model.id},
                )
            )
    else:
        form = DataSourceForm()
    return render(request, "sources/create.html", {"form": form})


@login_required
def sources_imports_create(request, slug: str, id: int):
    try:
        source = DataSource.objects.prefetch_related("dataimport_set").get(
            pk=id, user=request.user
        )
    except DataSource.DoesNotExist:
        return HttpResponseRedirect(reverse("website/sources"))

    if request.method == "POST":
        form = DataImportForm(request.POST, request.FILES)
        if form.is_valid():
            model = form.save(commit=False)
            model.user = request.user
            model.data_source = source
            model.file_name = request.FILES["file"].name
            model.save()
            return HttpResponseRedirect(
                reverse(
                    "website/sources/import/configure",
                    kwargs={
                        "source_slug": source.slug(),
                        "source_id": source.id,
                        "import_id": model.id,
                    },
                )
            )
    else:
        form = DataImportForm()
    return render(request, "sources/import_create.html", {"form": form})


@login_required
def sources_imports_configure(
    request, source_slug: str, source_id: int, import_id: int
):
    try:
        source = DataSource.objects.get(pk=source_id, user=request.user)
    except DataSource.DoesNotExist:
        return HttpResponseRedirect(reverse("website/sources"))

    try:
        imprt = DataImport.objects.select_related("csv_format").get(
            pk=import_id,
            user=request.user,
            data_source=source,
        )
    except DataSource.DoesNotExist:
        return HttpResponseRedirect(
            reverse(
                "website/sources/view", kwargs={"slug": source.slug, "id": source.id}
            )
        )

    csv_format = imprt.csv_format or DataImportCSVFormat()

    if (
        request.method == "POST"
        and "form" in request.POST
        and request.POST["form"] == "config"
    ):
        form_format = DataImportCSVFormatForm(request.POST, instance=csv_format)
        if form_format.is_valid():
            form_format.save()
            imprt.csv_format = csv_format
            imprt.save()

            return HttpResponseRedirect(
                reverse(
                    "website/sources/import/configure",
                    kwargs={
                        "source_slug": source_slug,
                        "source_id": source_id,
                        "import_id": import_id,
                    },
                )
            )
    else:
        form_format = DataImportCSVFormatForm(instance=csv_format)

    errors = []
    warnings = []

    info = csv_processor.get_info(imprt.file, csv_format)

    errors += info["errors"]
    warnings += info["warnings"]

    formset_columns_initial = [
        {"enabled": len(column["valid_types"]) > 0} for column in info["columns"]
    ]

    if (
        request.method == "POST"
        and "form" in request.POST
        and request.POST["form"] == "columns"
    ):
        formset_columns = DataImportCSVColumnFormSet(
            initial=formset_columns_initial, data=request.POST
        )

        try:
            columns = {
                col_info["name"]: form["data_format"].value()
                for col_info, form in zip(info["columns"], formset_columns)
                if form["enabled"].value()
            }
            _do_import(imprt, info["columns"], columns)

            return HttpResponseRedirect(
                reverse(
                    "website/sources/view",
                    kwargs={"slug": source_slug, "id": source_id},
                )
            )
        except Exception as e:
            errors.append(f"Error importing file: {e}")
    else:
        formset_columns = DataImportCSVColumnFormSet(initial=formset_columns_initial)

    columns = [
        {
            "form": form,
            "meta": meta,
        }
        for form, meta in zip(formset_columns, info["columns"])
    ]

    return render(
        request,
        "sources/import_configure.html",
        {
            "source": source,
            "import": imprt,
            "errors": errors,
            "bad_format": len(info["errors"]) > 0,
            "warnings": warnings,
            "form_format": form_format,
            "row_count": info["row_count"],
            "columns_management": formset_columns.management_form,
            "columns": columns,
        },
    )


@transaction.atomic
def _do_import(imprt: DataImport, info: dict, columns: dict):
    # TODO: Check columns (exactly one timestamp, etc...)

    imprt.processed_at = Now()
    imprt.save()

    # Delete previous import, if there is one
    # TODO: Performance
    DataPointNumeric.objects.filter(data_import=imprt).delete()

    timestamp_columns = [
        key for key, value in columns.items() if value.startswith("timestamp")
    ]
    data_columns = [
        key for key, value in columns.items() if not value.startswith("timestamp")
    ]

    if len(timestamp_columns) != 1:
        raise ValueError("Exactly one timestamp column must be selected")

    timestamp_column = timestamp_columns[0]

    if len(data_columns) == 0:
        raise ValueError("No data columns are selected")

    # TODO: Filter once we have other column types
    numeric_columns = data_columns

    numeric_column_objects = [
        DataSourceColumnNumeric.objects.get_or_create(
            data_source=imprt.data_source,
            name=name,
            defaults={"order": i, "unit": ""},
        )[0]
        for i, name in enumerate(numeric_columns)
    ]

    for batch in batched(
        csv_processor.iter_rows(imprt.file, imprt.csv_format, info, columns), 1000
    ):
        data_points = []
        for row in batch:
            timestamp = row[timestamp_column]
            if timestamp is None:
                continue

            for name, column in zip(numeric_columns, numeric_column_objects):
                data = row[name]
                if data is None:
                    continue

                data_points.append(
                    DataPointNumeric(
                        timestamp=timestamp,
                        value=data,
                        data_import=imprt,
                        column=column,
                    )
                )

        DataPointNumeric.objects.bulk_create(data_points)
