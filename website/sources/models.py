from django.db import models
from django.utils.text import slugify

from website.models import User


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/imports/<filename>
    return f"user_{instance.user.id}/imports/{filename}"


class DataSource(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def slug(self):
        return slugify(self.name)

    def __str__(self):
        return self.name


class DataImportCSVFormat(models.Model):
    dialect = models.CharField(max_length=100, default="excel")
    skip_rows = models.IntegerField(default=0)


class DataImport(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    file_name = models.CharField(max_length=100)
    file = models.FileField(upload_to=user_directory_path)
    data_source = models.ForeignKey(DataSource, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    processed_at = models.DateTimeField(null=True)

    csv_format = models.ForeignKey(
        DataImportCSVFormat, null=True, on_delete=models.SET_NULL
    )
    timestamp_column = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.file_name


class DataSourceColumnNumeric(models.Model):
    data_source = models.ForeignKey(DataSource, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    order = models.FloatField()
    unit = models.CharField(max_length=100)


class DataPointNumeric(models.Model):
    timestamp = models.DateTimeField()
    value = models.DecimalField(max_digits=20, decimal_places=5)
    data_import = models.ForeignKey(DataImport, on_delete=models.CASCADE)
    column = models.ForeignKey(DataSourceColumnNumeric, on_delete=models.CASCADE)
