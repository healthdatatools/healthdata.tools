from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth import get_user_model
from website_test_common import cleanup_uploaded_files

NO_SOURCES_MSG = "You don't have any data sources. Why not create one?"
NO_IMPORTS_MSG = "You haven't imported any data for this source yet."


class ImportTest(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user", password="pass")
        self.client.login(username="user", password="pass")

        response = self.client.post("/sources/create", {"name": "MY SOURCE NAME"})
        self.assertEqual(response.status_code, 302)
        self.source_page = response.headers.get("Location")

    def tearDown(self):
        cleanup_uploaded_files()

    def test_upload_invalid_file(self):
        content = b"\xff\x00"
        file = SimpleUploadedFile("file.csv", content=content, content_type="text/csv")
        response = self.client.post(
            f"{self.source_page}/import/create", {"file": file}, follow=True
        )
        self.assertContains(response, "File scanned with 1 errors and 0 warnings")
        self.assertContains(response, "Error processing file:")

    def test_upload_no_rows(self):
        content = b"a,b,c"
        file = SimpleUploadedFile("file.csv", content=content, content_type="text/csv")
        response = self.client.post(
            f"{self.source_page}/import/create", {"file": file}, follow=True
        )
        self.assertContains(response, "File scanned with 1 errors and 0 warnings")
        self.assertContains(response, "File has no rows")

    def test_upload_no_columns(self):
        content = b"\r\n\r\n\r\n"
        file = SimpleUploadedFile("file.csv", content=content, content_type="text/csv")
        response = self.client.post(
            f"{self.source_page}/import/create", {"file": file}, follow=True
        )
        self.assertContains(response, "File scanned with 1 errors and 0 warnings")
        self.assertContains(response, "File has no columns")

    def test_upload_generate_columns(self):
        content = b"""column_a,column_b,column_c
1,2,3,4,5"""
        file = SimpleUploadedFile("file.csv", content=content, content_type="text/csv")
        response = self.client.post(
            f"{self.source_page}/import/create", {"file": file}, follow=True
        )
        self.assertContains(response, "File scanned with 0 errors and 1 warnings")
        self.assertContains(response, "2 columns did not have labels")
        self.assertContains(response, "File has 5 columns and 1 rows")
        self.assertContains(response, "column_a")
        self.assertContains(response, "column_b")
        self.assertContains(response, "column_c")
        self.assertNotContains(response, "column_1")
        self.assertNotContains(response, "column_2")
        self.assertNotContains(response, "column_3")
        self.assertContains(response, "column_4")
        self.assertContains(response, "column_5")

    def test_upload_skip(self):
        content = b"""skipme_1
skipme_2
column_a,column_b,column_c
1,2,3
4,5,6
7,8,9
10,11,12"""
        file = SimpleUploadedFile("file.csv", content=content, content_type="text/csv")
        response = self.client.post(
            f"{self.source_page}/import/create", {"file": file}, follow=True
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            response.redirect_chain[-1][0],
            {"form": "config", "dialect": "excel", "skip_rows": 2},
            follow=True,
        )
        self.assertContains(response, "File scanned with 0 errors and 0 warnings")
        self.assertContains(response, "File has 3 columns and 4 rows")

    def test_upload_duplicate_columns(self):
        content = b"""My Column,Something,My Column
1,2,3
4,5,6"""
        file = SimpleUploadedFile("file.csv", content=content, content_type="text/csv")
        response = self.client.post(
            f"{self.source_page}/import/create", {"file": file}, follow=True
        )
        self.assertContains(response, "File scanned with 0 errors and 1 warnings")
        self.assertContains(response, "Column &quot;My Column&quot; appears 2 times")
        self.assertContains(response, "File has 3 columns and 2 rows")
        self.assertContains(response, 'Column "My Column"')
        self.assertContains(response, 'Column "Something"')
