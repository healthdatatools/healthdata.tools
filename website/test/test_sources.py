from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth import get_user_model
from website_test_common import cleanup_uploaded_files

NO_SOURCES_MSG = "You don't have any data sources. Why not create one?"
NO_IMPORTS_MSG = "You haven't imported any data for this source yet."


class SourceTest(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user", password="pass")
        self.client.login(username="user", password="pass")

    def tearDown(self):
        cleanup_uploaded_files()

    def test_source_not_found(self):
        response = self.client.get("/sources/nope-999")
        self.assertRedirects(response, "/sources")

    def test_source_different_user(self):
        response = self.client.post("/sources/create", {"name": "MY SOURCE NAME"})
        self.assertEqual(response.status_code, 302)
        source_page = response.headers.get("Location")

        # User who created can access
        response = self.client.get(source_page)
        self.assertContains(response, "MY SOURCE NAME")

        # Other users cannot
        User = get_user_model()
        User.objects.create_user(username="user2", password="pass")
        self.client.login(username="user2", password="pass")

        response = self.client.get(source_page)
        self.assertRedirects(response, "/sources")

    def test_no_sources(self):
        response = self.client.get("/sources")
        self.assertContains(response, NO_SOURCES_MSG)

    def test_create_source(self):
        response = self.client.get("/sources")
        self.assertContains(response, NO_SOURCES_MSG)

        response = self.client.post("/sources/create", {"name": "MY SOURCE NAME"})
        self.assertEqual(response.status_code, 302)
        source_page = response.headers.get("Location")

        response = self.client.get(source_page)
        self.assertContains(response, "MY SOURCE NAME")

        response = self.client.get("/sources")
        self.assertContains(response, "MY SOURCE NAME")

    def test_upload_import(self):
        response = self.client.post("/sources/create", {"name": "MY SOURCE NAME"})
        self.assertEqual(response.status_code, 302)
        source_page = response.headers.get("Location")

        response = self.client.get(source_page)
        self.assertContains(response, "MY SOURCE NAME")
        self.assertContains(response, NO_IMPORTS_MSG)

        # Test uploads with empty file
        file = SimpleUploadedFile("file.csv", content=b"", content_type="text/csv")
        response = self.client.post(f"{source_page}/import/create", {"file": file})
        self.assertContains(response, "The submitted file is empty.")

        # Test uploads with non-csv file
        file = SimpleUploadedFile(
            "file.txt", content=b"hello", content_type="text/plain"
        )
        response = self.client.post(f"{source_page}/import/create", {"file": file})
        self.assertContains(response, "Please upload a .csv file.")

        # Test uploads with a too-large file
        file = SimpleUploadedFile(
            "file.csv", content=b"A" * (256 * 1024 * 1024 + 1), content_type="text/csv"
        )
        response = self.client.post(
            f"{source_page}/import/create",
            {"file": file},
        )
        self.assertContains(response, "Maximum file size is 256 MB.")

        # Test uploads with good file
        file = SimpleUploadedFile("file.csv", content=b"a,b,c", content_type="text/csv")
        response = self.client.post(
            f"{source_page}/import/create",
            {"file": file},
        )
        self.assertEqual(response.status_code, 302)

        response = self.client.get(source_page)
        self.assertContains(response, "MY SOURCE NAME")
        self.assertNotContains(response, NO_IMPORTS_MSG)
        self.assertContains(response, "file.csv")
