import json
from django.test import TestCase

from website.charts.views import calculate_rolling_avg, downsample


class PandasTest(TestCase):
    def setUp(self):
        pass

    def test_downsampleTest(self):
        with open("website/test/GlucoseData.json", "r") as file:
            test_data = json.load(file)

        downsampled_data = downsample(test_data, frequency="D")
        self.assertEqual(len(downsampled_data), 5)

    def test_rollingAvgTest(self):
        with open("website/test/GlucoseData.json", "r") as file:
            test_data = json.load(file)
        window_size = 7
        size_before = len(test_data)
        rolling_avg = calculate_rolling_avg(test_data, window_size)
        size_after = len(rolling_avg)
        self.assertEqual(size_before, size_after)
        print(rolling_avg)
