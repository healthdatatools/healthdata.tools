from django.test import TestCase
import unittest

from django.contrib.auth import get_user_model


# Create your tests here.
# Ref: https://docs.djangoproject.com/en/5.0/topics/testing/overview/
class ExampleTestNoDatabase(unittest.TestCase):
    def setUp(self):
        pass

    def testHelloWorld(self):
        mystr = "Hello, test-driven development!"
        self.assertEqual("Hello, test-driven development!", mystr)


class ExampleTestThatDependsOnTheDatabase(TestCase):
    def setUp(self):
        # TODO: create objects in DB, etc...
        pass

    def testHelloWorld(self):
        mystr = "Hello, database-using world!"
        self.assertEqual("Hello, database-using world!", mystr)


class ProfileAccessTest(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user", password="pass")

    def test_no_auth_redirects(self):
        response = self.client.get("/profile/", follow=True)
        self.assertRedirects(response, "/accounts/login/?next=/profile/")

    def test_auth_redirects(self):
        self.client.login(username="user", password="pass")
        response = self.client.get("/profile/")
        self.assertEqual(response.status_code, 200)
