import pandas as pd
import numpy as np


def generate_health_data(start, end, granularity):
    date_range = pd.date_range(start=start, end=end, freq=granularity)

    timestamps = []
    heart_rates = []
    systolic_bps = []
    diastolic_bps = []
    spo2s = []

    # base values
    base_heart_rate = 70
    base_systolic_bp = 120
    base_diastolic_bp = 80
    base_spo2 = 98

    prev_heart_rate = base_heart_rate
    prev_systolic_bp = base_systolic_bp
    prev_diastolic_bp = base_diastolic_bp
    prev_spo2 = base_spo2

    for timestamp in date_range:
        # Use prev + step to generate new values
        heart_rate = prev_heart_rate + np.random.normal(0, 2)
        systolic_bp = prev_systolic_bp + np.random.normal(0, 1.5)
        diastolic_bp = prev_diastolic_bp + np.random.normal(0, 1)
        spo2 = prev_spo2 + np.random.normal(0, 0.5)

        heart_rate = max(40, min(heart_rate, 120))
        systolic_bp = max(90, min(systolic_bp, 140))
        diastolic_bp = max(60, min(diastolic_bp, 90))
        spo2 = max(94, min(spo2, 100))

        prev_heart_rate = heart_rate
        prev_systolic_bp = systolic_bp
        prev_diastolic_bp = diastolic_bp
        prev_spo2 = spo2

        timestamps.append(timestamp)
        heart_rates.append(heart_rate)
        systolic_bps.append(systolic_bp)
        diastolic_bps.append(diastolic_bp)
        spo2s.append(spo2)

    data = {
        "timestamp": timestamps,
        "heart_rate": heart_rates,
        "systolic_bp": systolic_bps,
        "diastolic_bp": diastolic_bps,
        "spo2": spo2s,
    }
    df = pd.DataFrame(data)

    return df


start_time = "2023-01-01 00:00:00"
end_time = "2023-01-02 00:00:00"
granularity = "1min"

health_data = generate_health_data(start_time, end_time, granularity)
print(health_data)
health_data.to_csv("health_data.csv", index=False)
