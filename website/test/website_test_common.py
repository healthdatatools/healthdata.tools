import os
from django.conf import settings
from website.sources.models import DataImport


def cleanup_uploaded_files():
    for i in DataImport.objects.all():
        path = os.path.join(settings.MEDIA_ROOT, i.file.name)
        os.remove(path)
