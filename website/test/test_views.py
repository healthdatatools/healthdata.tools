from django.test import TestCase
from django.contrib.auth import get_user_model
from website.sources.models import (
    DataSource,
    DataImport,
    DataSourceColumnNumeric,
    DataPointNumeric,
)
from django.utils import timezone


class AccessTest(TestCase):

    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user", password="pass")

    def test_no_auth_redirects(self):
        response = self.client.get("/profile/", follow=True)
        self.assertRedirects(response, "/accounts/login/?next=/profile/")

    def test_auth_redirects(self):
        self.client.login(username="user", password="pass")
        response = self.client.get("/profile/")
        self.assertEqual(response.status_code, 200)


class ChartsViewTest(TestCase):

    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user", password="pass")
        self.client.login(username="user", password="pass")

    def test_get_sources_view_no_data_sources(self):
        response = self.client.get("/get_sources/", follow=True)
        self.assertEqual(response.status_code, 200)
        response_data = response.json()
        self.assertEqual(len(response_data), 0)

    def test_get_sources_view_with_data_sources(self):
        data_source = DataSource.objects.create(
            user=self.user, name="Test Source", created_at=timezone.now()
        )
        DataImport.objects.create(
            user=self.user,
            data_source=data_source,
            file_name="test.csv",
        )
        response = self.client.get("/get_sources/", follow=True)
        self.assertEqual(response.status_code, 200)
        response_data = response.json()
        self.assertEqual(len(response_data), 1)


class TimeseriesDataApiTest(TestCase):

    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="testuser", password="password")
        self.client.login(username="testuser", password="password")
        self.data_source = DataSource.objects.create(
            user=self.user, name="Test Source", created_at=timezone.now()
        )

    def test_get_timeseries_data_valid_source(self):
        data_import = DataImport.objects.create(
            user=self.user,
            data_source=self.data_source,
            file_name="test.csv",
        )
        column = DataSourceColumnNumeric.objects.create(
            data_source=self.data_source, name="Column 1", order=1, unit="u"
        )
        DataPointNumeric.objects.create(
            timestamp=timezone.now(), value=10.0, data_import=data_import, column=column
        )
        first_timestamp = (
            DataPointNumeric.objects.filter(column=column).order_by("timestamp").first()
        ).timestamp.strftime("%Y-%m-%d")
        last_timestamp = (
            DataPointNumeric.objects.filter(column=column)
            .order_by("-timestamp")
            .first()
        ).timestamp.strftime("%Y-%m-%d")
        response = self.client.post(
            "/get_timeseries_data/"
            + str(self.data_source.id)
            + "/"
            + str(column.id)
            + "/",
            data={
                "transform": 0,
                "frequency": "",
                "windowSize": 0,
                "startDate": first_timestamp,
                "endDate": last_timestamp,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        json_data = response.json()
        self.assertEqual(json_data["sourceName"], self.data_source.name)
        self.assertEqual(
            json_data["unit"],
            DataSourceColumnNumeric.objects.filter(data_source=self.data_source)
            .first()
            .unit,
        )
        self.assertIn("data", json_data)

    def test_get_timeseries_data_invalid_source(self):
        response = self.client.get(
            "/get_timeseries_data/" + str(9999) + "/" + str(9999) + "/"
        )
        self.assertEqual(response.status_code, 404)
        json_data = response.json()
        self.assertEqual(json_data["error"], "Data source not found")

    def test_get_timeseries_data_invalid_data_column(self):
        DataImport.objects.create(
            user=self.user,
            data_source=self.data_source,
            file_name="test.csv",
        )
        response = self.client.get(
            "/get_timeseries_data/" + str(self.data_source.id) + "/" + str(9999) + "/"
        )
        self.assertEqual(response.status_code, 404)
        json_data = response.json()
        self.assertEqual(json_data["error"], "Data column not found")

    def test_get_timeseries_data_no_data_points(self):
        DataImport.objects.create(
            user=self.user,
            data_source=self.data_source,
            file_name="test.csv",
        )
        column = DataSourceColumnNumeric.objects.create(
            data_source=self.data_source, name="Column 1", order=1, unit="u"
        )
        response = self.client.get(
            "/get_timeseries_data/"
            + str(self.data_source.id)
            + "/"
            + str(column.id)
            + "/"
        )
        self.assertEqual(response.status_code, 404)
        json_data = response.json()
        self.assertEqual(
            json_data["error"], "No data points found for the specified column"
        )
