from django import forms
from .models import ChartSettings


class ChartSettingsForm(forms.ModelForm):
    class Meta:
        model = ChartSettings
        fields = [
            "name",
            "series_color",
            "line_weight",
            "line_type",
            "start_value",
            "end_value",
        ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

    def clean_name(self):
        name = self.cleaned_data["name"]
        if not name.strip():
            raise forms.ValidationError("Preset name is required.")

        if ChartSettings.objects.filter(user=self.user, name=name).exists():
            raise forms.ValidationError(
                f"A preset with the name '{name}' already exists."
            )
        return name

    def clean(self):
        data = super().clean()
        start_value = data.get("start_value")
        end_value = data.get("end_value")

        if (
            start_value is not None
            and end_value is not None
            and start_value >= end_value
        ):
            raise forms.ValidationError("Start value must be less than end value.")

        return data
