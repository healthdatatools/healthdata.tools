from django.db import models
from website.models import User


class ChartSettings(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    start_value = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    end_value = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    series_color = models.CharField(max_length=7, default="#000000")
    line_weight = models.IntegerField(default=1)
    line_type = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user", "name"], name="unique_preset_name_per_user"
            ),
        ]

    def __str__(self):
        return f"{self.user} - Setting {self.id}"
