import json
from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils import timezone
import pandas as pd
from website.charts.forms import ChartSettingsForm
from website.charts.models import ChartSettings
from website.sources.models import (
    DataSource,
    DataSourceColumnNumeric,
    DataPointNumeric,
)


@login_required
def charts(request):
    return render(request, "website/charts.html")


@login_required
def get_sources(request):
    sources = DataSource.objects.order_by("created_at").filter(user=request.user)
    sources_data = [
        {"id": source.id, "name": source.name, "created_at": source.created_at}
        for source in sources
    ]
    return JsonResponse(sources_data, safe=False)


@login_required
def get_presets(request):
    presets = ChartSettings.objects.order_by("name").filter(user=request.user)
    presets_data = [{"id": preset.id, "name": preset.name} for preset in presets]
    return JsonResponse(presets_data, safe=False)


@login_required
def get_columns(request, source_id):
    columns = DataSourceColumnNumeric.objects.filter(data_source_id=source_id).order_by(
        "order"
    )
    column_data = []

    for column in columns:
        first_timestamp = (
            DataPointNumeric.objects.filter(column=column).order_by("timestamp").first()
        )
        last_timestamp = (
            DataPointNumeric.objects.filter(column=column)
            .order_by("-timestamp")
            .first()
        )
        column_data.append(
            {
                "id": column.id,
                "name": column.name,
                "first_timestamp": (
                    first_timestamp.timestamp.strftime("%Y-%m-%d")
                    if first_timestamp
                    else None
                ),
                "last_timestamp": (
                    last_timestamp.timestamp.strftime("%Y-%m-%d")
                    if last_timestamp
                    else None
                ),
            }
        )

    return JsonResponse(column_data, safe=False)


@login_required
def get_timeseries_data(request, source_id, column_id):
    if request.method == "POST":
        data = json.loads(request.body)
        transform = int(data.get("transform", 0))
        frequency = data.get("frequency", "D")
        window_size = int(data.get("windowSize", 7))
        start_date = data.get("startDate", None)
        end_date = data.get("endDate", None)
    try:
        data_source = DataSource.objects.get(id=source_id, user=request.user)
    except DataSource.DoesNotExist:
        return JsonResponse({"error": "Data source not found"}, status=404)

    try:
        data_column = DataSourceColumnNumeric.objects.get(
            id=column_id, data_source=data_source
        )
    except DataSourceColumnNumeric.DoesNotExist:
        return JsonResponse({"error": "Data column not found"}, status=404)

    data_points = (
        DataPointNumeric.objects.filter(column=data_column)
        .order_by("timestamp")
        .values("timestamp", "value")
    )

    if not data_points.exists():
        return JsonResponse(
            {"error": "No data points found for the specified column"}, status=404
        )
    if start_date and end_date:
        start_date = timezone.make_aware(pd.to_datetime(start_date))
        end_date = timezone.make_aware(pd.to_datetime(end_date))
        data_points = data_points.filter(timestamp__range=(start_date, end_date))
    formatted_data = [
        {
            "timestamp": data_point["timestamp"].strftime("%m/%d/%Y %H:%M"),
            "value": float(data_point["value"]),
        }
        for data_point in data_points
    ]
    if transform == 1:
        formatted_data = calculate_rolling_avg(
            downsample(formatted_data, frequency), window_size
        )
    elif transform == 2:
        print("downsample" + frequency)
        formatted_data = downsample(formatted_data, frequency)
    # Prepare the response object
    response = {
        "sourceName": data_source.name,
        "columnName": data_column.name,
        "data": formatted_data,
        "unit": data_column.unit,
    }
    return JsonResponse(response, safe=False)


@login_required
def save_chart_settings(request):
    if request.method == "POST":
        data = json.loads(request.body)
        form = ChartSettingsForm(data, user=request.user)
        if form.is_valid():
            model = form.save(commit=False)
            model.user = request.user
            model.save()
            return JsonResponse(
                {"success": True, "message": "Settings saved successfully"}
            )
        else:
            return JsonResponse({"success": False, "errors": form.errors}, status=400)
    return JsonResponse({"success": False, "message": "Invalid request"}, status=400)


@login_required
def update_chart_settings(request, setting_id):
    if request.method == "PUT":
        data = json.loads(request.body)
        model = ChartSettings.objects.get(id=setting_id, user=request.user)
        model.name = data["name"]
        model.series_color = data["series_color"]
        model.line_weight = data["line_weight"]
        model.line_type = data["line_type"]
        model.start_value = data["start_value"]
        model.end_value = data["end_value"]
        model.save()
        return JsonResponse({"status": "success"}, status=200)
    return JsonResponse({"success": False, "message": "Invalid request"}, status=400)


@login_required
def load_chart_settings(request, setting_id):
    try:
        settings = ChartSettings.objects.get(id=setting_id, user=request.user)
    except ChartSettings.DoesNotExist:
        return JsonResponse({"error": "Setting not found"}, status=404)
    response = {
        "id": settings.id,
        "name": settings.name,
        "series_color": settings.series_color,
        "line_weight": settings.line_weight,
        "line_type": settings.line_type,
        "start_value": settings.start_value,
        "end_value": settings.end_value,
    }
    return JsonResponse(response, safe=False)


"""
Downsample a list of dictionary of DataPointNumeric to a specified frequency
Args:
    DataNumeric list of dictionary pts : the timeseries dictionary to downsample (must have dict 'timestamp and 'value')
    freqency: the downsample frequency, 'D': Daily, 'W': Weekly, 'H': Hourly, 'min': Minutes, and 'S': Seconds
    int rounding: the decimal place to round the output to (0 by default)
Returns:
    list of dictionary: the downsampled timeseries
"""


def downsample(pts, frequency, rounding=0):
    df = pd.DataFrame(pts)
    df["timestamp"] = pd.to_datetime(df["timestamp"])
    df.set_index("timestamp", inplace=True)
    df_downsampled = df.resample(frequency).mean()
    df_downsampled = df_downsampled.round(rounding).astype(int)
    downsampled_list = (
        df_downsampled.reset_index()
        .apply(
            lambda row: {
                "timestamp": row["timestamp"].strftime("%m/%d/%Y %H:%M"),
                "value": row["value"],
            },
            axis=1,
        )
        .tolist()
    )
    return downsampled_list


"""
Calculate the rolling average of a list of dictionary of DataPointNumeric
Args:
    DataNumeric list of dictionary pts : the timeseries dictionary to calculate on (must have dict 'timestamp and 'value')
    int window_size: the window size to calculate the rolling average
Returns:
    list of dictionary: the timeseries with the rolling average
"""


def calculate_rolling_avg(data, window_size):
    df = pd.DataFrame(data)
    df = df.dropna()
    df["timestamp"] = pd.to_datetime(df["timestamp"])
    df.set_index("timestamp", inplace=True)
    df["rolling_avg"] = df["value"].rolling(window=window_size).mean().round(0)
    df["rolling_avg"] = df["rolling_avg"].fillna(df["value"])
    rolling_avg = (
        df.reset_index()
        .apply(
            lambda row: {
                "timestamp": row["timestamp"].strftime("%m/%d/%Y %H:%M"),
                "value": row["rolling_avg"],
            },
            axis=1,
        )
        .tolist()
    )
    return rolling_avg
