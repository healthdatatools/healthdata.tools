from django.contrib import admin
from .models import ChartSettings


class ChartSettingsAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "name",
        "series_color",
        "line_weight",
        "line_type",
        "updated_at",
        "start_value",
        "end_value",
    )


admin.site.register(ChartSettings)
