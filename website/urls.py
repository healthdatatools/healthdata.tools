from django.urls import include, path
from django.contrib.auth.views import LoginView
from django_registration.backends.activation.views import RegistrationView

from .forms import CustomRegistrationForm

from .views import about, index, profile
from .sources.views import (
    sources_list,
    sources_create,
    sources_view,
    sources_imports_create,
    sources_imports_configure,
)
from .charts.views import (
    charts,
    get_sources,
    get_presets,
    get_columns,
    get_timeseries_data,
    load_chart_settings,
    save_chart_settings,
    update_chart_settings,
)

urlpatterns = [
    path("", index, name="website/home"),
    path("about", about, name="website/about"),
    path("sources", sources_list, name="website/sources"),
    path("sources/create", sources_create, name="website/sources/create"),
    path("sources/<slug:slug>-<int:id>", sources_view, name="website/sources/view"),
    path(
        "sources/<slug:slug>-<int:id>/import/create",
        sources_imports_create,
        name="website/sources/import/create",
    ),
    path(
        "sources/<slug:source_slug>-<int:source_id>/import/<int:import_id>/configure",
        sources_imports_configure,
        name="website/sources/import/configure",
    ),
    path(
        "accounts/register/",
        RegistrationView.as_view(
            form_class=CustomRegistrationForm,
        ),
        name="django_registration_register",
    ),
    path(
        "accounts/login/",
        LoginView.as_view(redirect_authenticated_user=True),
        name="login",
    ),
    path("accounts/", include("django_registration.backends.activation.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("profile/", profile, name="website/profile"),
    path("charts/", charts, name="website/charts"),
    path("get_sources/", get_sources, name="get_sources"),
    path("get_presets/", get_presets, name="get_presets"),
    path("get_columns/<int:source_id>/", get_columns, name="get_columns"),
    path(
        "get_timeseries_data/<int:source_id>/<int:column_id>/",
        get_timeseries_data,
        name="get_timeseries_data",
    ),
    path(
        "save_chart_settings/",
        save_chart_settings,
        name="save_chart_settings",
    ),
    path(
        "load_chart_settings/<int:setting_id>/",
        load_chart_settings,
        name="load_chart_settings",
    ),
    path(
        "update_chart_settings/<int:setting_id>/",
        update_chart_settings,
        name="update_chart_settings",
    ),
]
