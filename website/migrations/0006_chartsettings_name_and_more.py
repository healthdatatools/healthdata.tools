# Generated by Django 5.1.2 on 2024-12-13 23:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("website", "0005_remove_chartsettings_preset_name"),
    ]

    operations = [
        migrations.AddField(
            model_name="chartsettings",
            name="name",
            field=models.CharField(default="deleteme", max_length=20),
            preserve_default=False,
        ),
        migrations.AddConstraint(
            model_name="chartsettings",
            constraint=models.UniqueConstraint(
                fields=("user", "name"), name="unique_preset_name_per_user"
            ),
        ),
    ]
