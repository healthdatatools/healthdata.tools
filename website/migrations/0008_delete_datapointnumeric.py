# Created manually to purge unmigratable data

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("website", "0007_merge_20250110_0254"),
    ]

    operations = [
        migrations.DeleteModel(
            name="DataPointNumeric",
        ),
    ]
