from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from website.charts.models import ChartSettings
from .sources.models import (
    DataSource,
    DataImport,
    DataSourceColumnNumeric,
    DataPointNumeric,
)
from .models import User


admin.site.register(User, UserAdmin)
admin.site.register(DataSource)
admin.site.register(DataImport)
admin.site.register(DataSourceColumnNumeric)
admin.site.register(DataPointNumeric)
admin.site.register(ChartSettings)
