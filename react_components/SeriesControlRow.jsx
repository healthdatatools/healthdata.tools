import React, { useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import TransformDropdown from "./TransformDropdown";
import StyleDialogButton from "./StyleDialogButton";
import { Box, Button, TextField } from "@mui/material";

const DEFAULT_TRANSFORM = "0";

const SeriesControlRow = ({
  sources,
  lastAppliedSettings,
  onRemove,
  onUpdate,
  style,
  setStyle,
}) => {
  const [columns, setColumns] = useState([]);
  const [selectedSource, setSelectedSource] = useState("");
  const [selectedColumn, setSelectedColumn] = useState("");
  const [selectedTransform, setSelectedTransform] = useState(DEFAULT_TRANSFORM);
  const [selectedFrequency, setSelectedFrequency] = useState("");
  const [selectedWindowSize, setSelectedWindowSize] = useState(0);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [minDate, setMinDate] = useState("");
  const [maxDate, setMaxDate] = useState("");
  // Fetch columns when source changes

  useEffect(() => {
    setSelectedSource(lastAppliedSettings.source ?? "");
    setSelectedColumn(lastAppliedSettings.column ?? "");
    setSelectedTransform(lastAppliedSettings.transform ?? DEFAULT_TRANSFORM);
    setSelectedFrequency(lastAppliedSettings.frequency ?? "");
    setSelectedWindowSize(lastAppliedSettings.windowSize ?? 0);
    setStartDate(lastAppliedSettings.startDate ?? "");
    setEndDate(lastAppliedSettings.endDate ?? "");
    setMinDate(lastAppliedSettings.minDate ?? "");
    setMaxDate(lastAppliedSettings.maxDate ?? "");
  }, [lastAppliedSettings]);

  useEffect(() => {
    if (selectedSource) {
      fetch(`/get_columns/${selectedSource}/`)
        .then((response) => response.json())
        .then((data) => {
          setColumns(data);
        })
        .catch((error) => {
          console.error("Error fetching columns:", error);
          setColumns([]);
        });
    } else {
      setColumns([]);
    }
    setSelectedColumn("");
  }, [selectedSource]);

  useEffect(() => {
    if (selectedColumn) {
      const selectedColumnData = columns.find(
        (col) => col.id == selectedColumn,
      );
      if (selectedColumnData) {
        setMinDate(selectedColumnData.first_timestamp || "");
        setMaxDate(selectedColumnData.last_timestamp || "");
        setStartDate(selectedColumnData.first_timestamp || "");
        setEndDate(selectedColumnData.last_timestamp || "");
      }
    }
  }, [selectedColumn, columns]);

  const handleApply = useCallback(() => {
    onUpdate({
      ...lastAppliedSettings,
      source: selectedSource,
      column: selectedColumn,
      transform: selectedTransform,
      frequency: selectedFrequency,
      windowSize: selectedWindowSize,
      startDate: startDate,
      endDate: endDate,
      minDate: minDate,
      maxDate: maxDate,
    });
  }, [
    onUpdate,
    lastAppliedSettings,
    selectedSource,
    selectedColumn,
    selectedTransform,
    selectedFrequency,
    selectedWindowSize,
    startDate,
    endDate,
    minDate,
    maxDate,
  ]);

  const areSettingsNotChanged = () => {
    return (
      selectedSource == lastAppliedSettings.source &&
      selectedColumn == lastAppliedSettings.column &&
      selectedTransform == lastAppliedSettings.transform &&
      selectedFrequency == lastAppliedSettings.frequency &&
      selectedWindowSize == lastAppliedSettings.windowSize &&
      startDate == lastAppliedSettings.startDate &&
      endDate == lastAppliedSettings.endDate
    );
  };

  return (
    <Box>
      <select
        value={selectedSource}
        onChange={(e) => setSelectedSource(e.target.value)}
        className="mb-4 p-2 border rounded"
      >
        <option value="">Select a data source</option>
        {sources.map((source) => (
          <option key={source.id} value={source.id}>
            {source.name}
          </option>
        ))}
      </select>

      <select
        value={selectedColumn}
        onChange={(e) => {
          {
            setSelectedColumn(e.target.value);
          }
        }}
        className="mb-4 p-2 border rounded"
        disabled={!columns.length}
      >
        <option value="">Select a column</option>
        {columns.map((column) => (
          <option key={column.id} value={column.id}>
            {column.name}
          </option>
        ))}
      </select>

      <TransformDropdown
        onTransformSelect={setSelectedTransform}
        onFrequencySelect={setSelectedFrequency}
        onWindowSizeSelect={setSelectedWindowSize}
        selectedTransform={selectedTransform}
        isDisabled={!selectedSource || !selectedColumn}
      />
      <TextField
        label="Start Date"
        type="date"
        value={startDate}
        onChange={(e) => setStartDate(e.target.value)}
        InputLabelProps={{
          shrink: true,
        }}
        slotProps={{
          input: {
            min: minDate,
            max: endDate || maxDate,
          },
        }}
        className="mb-4 p-2 border rounded"
        isDisabled={!selectedSource || !selectedColumn}
      />

      <TextField
        label="End Date"
        type="date"
        value={endDate}
        onChange={(e) => setEndDate(e.target.value)}
        InputLabelProps={{
          shrink: true,
        }}
        slotProps={{
          input: {
            min: startDate || minDate,
            max: maxDate,
          },
        }}
        className="mb-4 p-2 border rounded"
        isDisabled={!selectedSource || !selectedColumn}
      />
      <Button
        onClick={handleApply}
        disabled={!selectedSource || !selectedColumn || areSettingsNotChanged()}
      >
        Apply
      </Button>

      <StyleDialogButton style={style} setStyle={setStyle} disabled={false} />

      <Button onClick={onRemove}>Remove</Button>
    </Box>
  );
};

SeriesControlRow.propTypes = {
  sources: PropTypes.array.isRequired,
  lastAppliedSettings: PropTypes.object.isRequired,
  onRemove: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired,
  setStyle: PropTypes.func.isRequired,
};

export default SeriesControlRow;
