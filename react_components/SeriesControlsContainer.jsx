import React from "react";
import PropTypes from "prop-types";
import SeriesControlRow from "./SeriesControlRow";
import { Box, Button } from "@mui/material";

const SeriesControlsContainer = ({ sources, seriesState }) => {
  return (
    <Box>
      {seriesState.seriesControls.map((row) => (
        <SeriesControlRow
          key={row.id}
          sources={sources}
          lastAppliedSettings={row}
          onRemove={() => seriesState.removeSeriesControl(row.id)}
          onUpdate={(control) =>
            seriesState.updateSeriesControl(row.id, control)
          }
          style={seriesState.seriesStyles[row.id]}
          setStyle={(newStyle) =>
            seriesState.updateSeriesStyle(row.id, newStyle)
          }
        />
      ))}
      <Button onClick={seriesState.addSeriesControl}>Add Series</Button>
    </Box>
  );
};

SeriesControlsContainer.propTypes = {
  sources: PropTypes.array.isRequired,
  seriesState: PropTypes.object.isRequired,
};

export default SeriesControlsContainer;
