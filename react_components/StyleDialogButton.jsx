import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";
import PropTypes from "prop-types";
import BasicCustomizationPanel from "./BasicCustomizationPanel";

const StyleDialogButton = ({ style, setStyle, disabled }) => {
  const [styleDialogOpen, setStyleDialogOpen] = useState(false);

  const openStyleDialog = () => {
    setStyleDialogOpen(true);
  };

  const closeStyleDialog = () => {
    setStyleDialogOpen(false);
  };

  return (
    <>
      <Button
        onClick={openStyleDialog}
        disabled={disabled}
        className="className=mb-4 p-2 border rounded"
      >
        Style
      </Button>
      <Dialog open={styleDialogOpen} onClose={closeStyleDialog}>
        <DialogTitle sx={{ textAlign: "center" }}>Style Settings</DialogTitle>
        <DialogContent sx={{ py: 0 }}>
          <BasicCustomizationPanel style={style} setStyle={setStyle} />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeStyleDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

StyleDialogButton.propTypes = {
  style: PropTypes.object.isRequired,
  setStyle: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default StyleDialogButton;
