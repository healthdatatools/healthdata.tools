import React, { useCallback } from "react";
import PropTypes from "prop-types";
import { Box, Input, Typography } from "@mui/material";

const ColorPicker = ({ color, onChange, onCommitChange }) => {
  const handleInputChange = useCallback(
    (event) => {
      const newColor = event.target.value;
      onChange(newColor);
    },
    [onChange],
  );

  const handleCommit = useCallback(
    (event) => {
      const newColor = event.target.value;
      return onCommitChange(newColor);
    },
    [onCommitChange],
  );

  return (
    <Box sx={{ display: "flex", alignItems: "center", mt: 1 }}>
      <Typography sx={{ marginRight: 2 }}>Chart Color:</Typography>
      <Box
        sx={{
          width: "40px",
          height: "40px",
          backgroundColor: color,
          borderRadius: "50%",
        }}
      >
        <Input
          type="color"
          value={color}
          onChange={handleInputChange}
          onBlur={handleCommit}
          sx={{
            width: "100%",
            height: "100%",
            opacity: 0,
          }}
        />
      </Box>
    </Box>
  );
};

ColorPicker.propTypes = {
  color: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onCommitChange: PropTypes.func.isRequired, // This is the function to be triggered after color selection
};

export default React.memo(ColorPicker);
