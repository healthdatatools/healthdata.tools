import React, { useState, useEffect, useCallback, useMemo } from "react";
import { Box } from "@mui/material";
import SeriesControlsContainer from "./SeriesControlsContainer.jsx";
import Chart from "./Chart.jsx";
import { makeDefaultStyles } from "./BasicCustomizationPanel.jsx";

function useSeriesState() {
  const [seriesControls, setSeriesControls] = useState([]);
  const [seriesStyles, setSeriesStyles] = useState({});
  const [seriesData, setSeriesData] = useState({});

  const fetchSeries = useCallback(
    async (control) => {
      console.log("fetchSeries(", control, ")");

      if (!(control.source && control.column && control.transform)) {
        return;
      }

      const id = control.id;
      console.log("Fetching ", id);
      seriesData[id]?.abort?.abort(); // Abort any in-progress fetches
      const abort = new AbortController();
      setSeriesData((seriesData) => ({
        ...seriesData,
        [id]: {
          status: "loading",
          abort,
        },
      }));

      try {
        const csrfToken = document
          .querySelector('meta[name="csrf-token"]')
          .getAttribute("content");
        const transformData = {
          transform: control.transform,
          frequency: control.frequency,
          windowSize: control.windowSize,
          startDate: control.startDate,
          endDate: control.endDate,
        };
        const response = await fetch(
          `/get_timeseries_data/${control.source}/${control.column}/`,
          {
            signal: abort.signal,
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "X-CSRFToken": csrfToken,
            },
            body: JSON.stringify(transformData),
          },
        );

        if (response.status != 200) {
          try {
            const error = await response.json().error;
            throw new Error(error);
          } catch {
            throw new Error(
              `Unexpected response status code ${response.status}`,
            );
          }
        }

        const data = await response.json();

        setSeriesData((seriesData) => ({
          ...seriesData,
          [id]: { status: "done", response: data },
        }));
      } catch (error) {
        setSeriesData((seriesData) => ({
          ...seriesData,
          [id]: { status: "error", error },
        }));
      }
    },
    [seriesData, setSeriesData],
  );

  const addSeriesControl = useCallback(() => {
    const id = Date.now();
    setSeriesControls((prev) => [...prev, { id }]);
    setSeriesStyles((prev) => ({ ...prev, [id]: makeDefaultStyles() }));
  }, [setSeriesControls]);

  const removeSeriesControl = useCallback(
    (id) => {
      setSeriesControls((prev) => prev.filter((row) => row.id !== id));
      setSeriesData((seriesData) => ({
        ...seriesData,
        [id]: undefined,
      }));
    },
    [setSeriesControls],
  );

  const updateSeriesControl = useCallback(
    (id, newSeriesControl) => {
      setSeriesControls((prev) =>
        prev.map((row) => (row.id == id ? newSeriesControl : row)),
      );
      fetchSeries(newSeriesControl);
    },
    [setSeriesControls, fetchSeries],
  );

  const updateSeriesStyle = useCallback(
    (id, style) => setSeriesStyles((prev) => ({ ...prev, [id]: style })),
    [setSeriesStyles],
  );

  return useMemo(
    () => ({
      seriesControls,
      seriesData,
      seriesStyles,
      addSeriesControl,
      removeSeriesControl,
      updateSeriesControl,
      updateSeriesStyle,
    }),
    [
      seriesControls,
      seriesData,
      seriesStyles,
      addSeriesControl,
      removeSeriesControl,
      updateSeriesControl,
      updateSeriesStyle,
    ],
  );
}

const Dashboard = () => {
  const [sources, setSources] = useState([]);

  useEffect(() => {
    fetch("/get_sources/")
      .then((response) => response.json())
      .then((data) => setSources(data))
      .catch((error) => console.error("Error fetching sources:", error));
  }, []);

  const seriesState = useSeriesState();

  return (
    <Box sx={{ display: "flex", flex: "1 1 auto" }}>
      {/* Chart saving will go here */}
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          flex: "0 0 auto", // Takes only the space it needs
          margin: 4,
        }}
      >
        Chart saving stuff
      </Box>

      {/* Chart and Series Controls Migrated */}
      <Box
        sx={{
          display: "flex",
          flex: "1 1 auto",
          flexDirection: "column",
          margin: 4,
        }}
      >
        <SeriesControlsContainer sources={sources} seriesState={seriesState} />

        <Box
          id="chart"
          className="bg-white p-4 shadow-lg rounded-lg"
          // sx={{
          //   flex: "1 1 auto",
          //   mr: 15,
          // }}
        >
          <Chart
            seriesIds={useMemo(
              () => seriesState.seriesControls.map((x) => x.id),
              [seriesState.seriesControls],
            )}
            seriesData={seriesState.seriesData}
            seriesStyles={seriesState.seriesStyles}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Dashboard;
