import React from "react";
import CustomSlider from "./CustomSlider";
import ColorPicker from "./ColorPicker";
import { Box } from "@mui/material";
import { useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";

const DEFAULT_LINE_WEIGHT = 5;
const DEFAULT_LINE_TYPE = 0;

export function makeDefaultStyles() {
  return {
    seriesColor: "#000000",
    lineWeight: DEFAULT_LINE_WEIGHT,
    lineType: DEFAULT_LINE_TYPE,
  };
}

const BasicCustomizationPanel = ({ style, setStyle }) => {
  const [lineWeight, setLineWeight] = useState(style.lineWeight);
  const [lineType, setLineType] = useState(style.lineType);
  const [seriesColor, setSeriesColor] = useState(style.seriesColor);

  useEffect(() => {
    setLineWeight(style.lineWeight);
    setLineType(style.lineType);
    setSeriesColor(style.seriesColor);
  }, [style]);

  return (
    <Box sx={{ minWidth: "400px" }}>
      <CustomSlider
        label={`Line Weight: ${lineWeight}`}
        value={lineWeight}
        min={1}
        max={10}
        step={1}
        onChange={setLineWeight}
        onCommitChange={useCallback(
          (value) => {
            setStyle({ ...style, lineWeight: value });
          },
          [setStyle, style],
        )}
      />
      <CustomSlider
        label="Line Type"
        value={lineType}
        min={0}
        max={10}
        onChange={setLineType}
        onCommitChange={useCallback(
          (value) => {
            setStyle({ ...style, lineType: value });
          },
          [setStyle, style],
        )}
      />

      <ColorPicker
        color={seriesColor}
        onChange={setSeriesColor}
        onCommitChange={useCallback(
          (value) => {
            setStyle({ ...style, seriesColor: value });
          },
          [setStyle, style],
        )}
      />
    </Box>
  );
};

BasicCustomizationPanel.propTypes = {
  style: PropTypes.object.isRequired,
  setStyle: PropTypes.func.isRequired,
};

export default BasicCustomizationPanel;
