import React, { useMemo } from "react";
import PropTypes from "prop-types";
import ApexChart from "react-apexcharts";

const Chart = ({ seriesIds, seriesData, seriesStyles }) => {
  const options = useMemo(
    () => ({
      chart: {
        animations: {
          enabled: false,
        },
      },
      xaxis: {
        type: "datetime",
        title: {
          text: "Time",
        },
      },
      stroke: {
        curve: "smooth",
        width: seriesIds.map((id) => seriesStyles[id].lineWeight),
        dashArray: seriesIds.map((id) => seriesStyles[id].lineType),
      },
      colors: seriesIds.map((id) => seriesStyles[id].seriesColor),
    }),
    [seriesIds, seriesStyles],
  );

  const series = useMemo(
    () =>
      seriesIds
        .map((id) => {
          const response = seriesData[id]?.response;
          if (!response) {
            return undefined;
          }

          const data = response.data.map((entry) => ({
            x: new Date(entry["timestamp"]),
            y: entry["value"],
          }));

          return {
            name: id,
            data,
            yaxis: {
              title: {
                text: response.unit,
              },
            },
            title: {
              text: `${response.sourceName} Data`,
              align: "center",
            },
            tooltip: {
              shared: true,
              intersect: false,
              x: {
                format: "dd MMM yyyy HH:mm",
              },
            },
          };
        })
        .filter((x) => x),
    [seriesIds, seriesData],
  );

  return <ApexChart type="line" options={options} series={series} />;
};

Chart.propTypes = {
  seriesIds: PropTypes.array.isRequired,
  seriesData: PropTypes.object.isRequired,
  seriesStyles: PropTypes.object.isRequired,
};

export default Chart;
