import React from "react";
import PropTypes from "prop-types";
import { Box, Slider, Typography } from "@mui/material";

const CustomSlider = ({
  label,
  value,
  min,
  max,
  step,
  onChange,
  onCommitChange,
}) => (
  <Box>
    <Typography>{label}</Typography>
    <Slider
      value={value}
      min={min}
      max={max}
      step={step}
      onChange={(e, newValue) => onChange(newValue)}
      onChangeCommitted={(e, newValue) => onCommitChange(newValue)}
    />
  </Box>
);

CustomSlider.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  onCommitChange: PropTypes.func.isRequired,
};

export default React.memo(CustomSlider);
