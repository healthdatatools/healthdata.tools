import React from "react";
import PropTypes from "prop-types";
const transforms = [
  { id: 1, name: "Rolling Average" },
  { id: 2, name: "Downsample" },
];
const frequencies = [
  { id: "D", name: "Daily" },
  { id: "W", name: "Weekly" },
  { id: "H", name: "Hourly" },
  { id: "min", name: "Minutes" },
  { id: "S", name: "Seconds" },
];
const TransformDropdown = ({
  onTransformSelect,
  isDisabled,
  onFrequencySelect,
  onWindowSizeSelect,
  selectedTransform,
}) => {
  return (
    <>
      <select
        onChange={(e) => onTransformSelect(e.target.value)}
        className="mb-4 p-2 border rounded"
        disabled={isDisabled}
      >
        <option value="0">Select a transform</option>
        {transforms.map((transform) => (
          <option key={transform.id} value={transform.id}>
            {transform.name}
          </option>
        ))}
      </select>
      {(selectedTransform == 1 || selectedTransform == 2) && (
        <select
          onChange={(e) => onFrequencySelect(e.target.value)}
          className="mb-4 p-2 border rounded"
        >
          <option value="">Select a frequency</option>
          {frequencies.map((frequency) => (
            <option key={frequency.id} value={frequency.id}>
              {frequency.name}
            </option>
          ))}
        </select>
      )}
      {selectedTransform == 1 && (
        <input
          className="mb-4 p-2 border rounded"
          type="number"
          onChange={(e) => {
            const value = Math.min(Math.max(e.target.value, 1), 30);
            onWindowSizeSelect(value);
          }}
          placeholder="Window size"
          min="1"
          max="30"
          title="Window size must be between 1 and 30."
        />
      )}
    </>
  );
};

TransformDropdown.propTypes = {
  onTransformSelect: PropTypes.func.isRequired,
  onFrequencySelect: PropTypes.func.isRequired,
  onWindowSizeSelect: PropTypes.func.isRequired,
  selectedTransform: PropTypes.number.isRequired,
  isDisabled: PropTypes.bool.isRequired,
};

export default TransformDropdown;
