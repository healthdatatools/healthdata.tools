import React, { useState } from "react";
import { Box, Button, TextField } from "@mui/material";
import PropTypes from "prop-types";

const TargetAreaPanel = ({ chartManager, id }) => {
  const [startValue, setStartValue] = useState("");
  const [endValue, setEndValue] = useState("");

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
        gap: 2,
        alignItems: "center",
      }}
    >
      <Button
        onClick={() => chartManager.addTargetZone(id, startValue, endValue)}
      >
        Create Target Area
      </Button>
      <TextField
        label="Start Value"
        type="number"
        value={startValue}
        onChange={(e) => setStartValue(e.target.value)}
      />
      <TextField
        label="End Value"
        type="number"
        value={endValue}
        onChange={(e) => setEndValue(e.target.value)}
      />
    </Box>
  );
};

TargetAreaPanel.propTypes = {
  chartManager: PropTypes.object.isRequired,
  id: PropTypes.number.isRequired,
};

export default TargetAreaPanel;
