import React, { StrictMode } from "react";
import ReactDom from "react-dom";
import Dashboard from "../ChartDashboard";

const domContainer = document.querySelector("#ChartDashboard");
const root = ReactDom.createRoot(domContainer);
root.render(
  <StrictMode>
    <Dashboard />
  </StrictMode>,
);
