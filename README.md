# HealthData.Tools

HealthData.Tools is an open-source website and suite of tools to aggregate, store, and analyze various types of health data. Today we have medical-grade devices everywhere, in our pockets, on our wrists, and sitting on our nightstands. Unfortunately, almost all of these store their data in proprietary ways and make the user jump through hoops to analyze the data at all, if that is even possible.

HealthData.Tools seeks to be a single place where many types of health data may be imported from the devices that collect them. It then provides visualization and analysis tools so users can own thier own data and draw useful and meaningful conclusions from it, sometimes even gleaning insights which are not possible from any one data source in isolation.

* We have a community [Code of Conduct](CODE_OF_CONDUCT.md)
* We have a [Contributor Guide](CONTRIBUTING.md)
* TODO: set up a Style Guide
* TODO: set up a Development Guide
* TODO: set up a Production Environment/Deployment Guide

# Status

Main Branch Pipeline: ![status](https://gitlab.com/healthdatatools/healthdata.tools/badges/main/pipeline.svg?ignore_skipped=true)

Main Branch Coverage: ![coverage](https://gitlab.com/healthdatatools/healthdata.tools/badges/main/coverage.svg)

Latest Release: ![latestrelease](https://gitlab.com/healthdatatools/healthdata.tools/-/badges/release.svg)
