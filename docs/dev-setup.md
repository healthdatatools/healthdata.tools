# Introduction

This project uses Django and several other Python libraries. Poetry is used to manage dependencies, and build and run tests. This guide will walk you through setting up your development environment on Linux.

# Dev Environment / Tools

## Python Interpreter

This project versions python explicitly using `pyenv`. You should always use the precise version of python used in CI and production to ensure you don't run into any problems. You can install pyenv by following the directions in [their documentation](https://github.com/pyenv/pyenv?tab=readme-ov-file#installation).

For Linux, that looks like this:

    $ curl https://pyenv.run > pyenv.sh
    # <examine pyenv.sh, ensure it doesn't look evil>
    $ bash ./pyenv.sh

You can confirm pyenv is installed like this:

    $ pyenv -v
    pyenv 2.4.3

You can then install the appropriate version of python by consulting `.python-version`

    $ cat .python-version
    3.12.4
    $ pyenv install 3.12.4

Now, when you run python with pyenv from the project directory, you should see the correct python version.

    $ pyenv exec python --version
    Python 3.12.4

If desired, you can instruct pyenv to make this the default python as well:

    $ pyenv global 3.12.4
    < you may need to restart your shell now >
    $ which python
    /home/cmyers/.pyenv/shims/python
    $ python --version
    Python 3.12.4

## pipx

`pipx` is a tool that allows you to install python applications globally, but using isolated dependencies (with virtual environments) and in userspace. You can install pipx on most Linux distros by doing something like:

    $ sudo apt install pipx

Once that is done, you should ensure `pipx` is on your path:

    $ which pipx
    /usr/bin/pipx

## poetry

`poetry` is a dependency management and build tool for Python. Install `poetry` like this:

    $ pipx install poetry

You may need to ensure pipx installed programs are on your path by running `pipx ensurepath`. Then, you may need to close your shell and re-open it, and you should see poetry on your path.

    $ which poetry
    /home/cmyers/.local/bin/poetry
    $ poetry --version
    Poetry (version 1.8.3)

You can enable completions for your appropriate shell by following the directions [here](https://python-poetry.org/docs/#enable-tab-completion-for-bash)

## django

`django` will be installed by poetry and versioned along with the project's other dependencies, but you might also need a copy installed "globally" for doing tasks outside the project itself (such as starting the project!). You can install a version of django with `pipx` also, like this:

    $ pipx instal django

You can then see that the install was successful:

    $ which django-admin
    /home/cmyers/.local/bin/django-admin
    $ django-admin --version
    4.2.13

However, you can just run django-admin directly using poetry like this:

    $ poetry run django-admin --version
    5.0.6

## Setup your local databse and user

Now you can create your local database and superuser account

    $ poetry run python manage.py migrate
    Operations to perform:
    Apply all migrations: admin, auth, contenttypes, sessions, website
    Running migrations:
        ...
    
    $ poetry run python manage.py createsuperuser
    Username: your_username
    Email address: your_email@example.com
    Password: [enter a password]
    Password (again): [enter a password]
    Superuser created successfully.

## Run the web server

You should now be able to run the webserver:

    $ poetry run python manage.py runserver
    #snip...
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

You can now go to http://127.0.0.1:8000/ in your browser and see the server running!

# Debug the build

The build happens in a container using an image. You can exactly replicate this build locally by using a tool like [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local). You can also fetch the container locally and poke around using something like this:

    $ docker run -v $(pwd):/build -it registry.gitlab.com/healthdatatools/healthdata.tools/main /bin/bash
    root@f02a586f0ca0:/# cd /build
    root@f02a586f0ca0:/build# ./bin/build.sh
    #...
    root@f02a586f0ca0:/build# ./bin/test.sh
    #...

You can point to an image from a different branch by replacing `main` from the above command. You can also build the image locally from the dockerfile in `image/Dockerfile`.

# Production Setup

On the prod box, I did the following to set things up:

* as root, Install pyenv and python 3.12.4
* as root, Instlal pipx
* as www-data user, use pipx to install poetry and uwsgi (a prod-only dependency)
    * If we use poetry to version uwsgi, then our CI container will need to have a C compiler, which is a whole mess - there is no clean way to specify [prod-only dependencies](https://github.com/python-poetry/poetry/issues/1264) in poetry.
* Place the [hdt-prod.service](../config/prod/hdt-prod.service) file in `/etc/systemd/system`.
* Place the [config/91-sudoers-www-data-service-restart](../config/91-sudoers-www-data-service-restart) file in `/etc/sudoers.d`
* Create `/var/www/healthdata.tools` and chown it to the `www-data` user.
* Git clone the repo in `/var/www/healthdata.tools/git`, manually set up the first version by rsyncing it to `/var/www/healthdata.tools/versions/<SHA>` and creating the symlink in `/var/www/healthdata.tools/current` pointing to it. Echo the SHA1 into `/var/www/healthdata.tools/current/version.txt`.
* Add a crontab entry for the www-data user to run the deploy script, something like: `*/5 * * * * /var/www/healthdata.tools/git/bin/deploy.sh 2>&1 >> /tmp/prod.hdt.log`
* start the service: `service hdt-prod start`


# Reference

To figure out how to get this set up, I referred to these documents:

* https://pipx.pypa.io/stable/ - how to install pipx
* https://python-poetry.org/docs/ - how to install poetry
* https://builtwithdjango.com/blog/basic-django-setup - how to use django with poetry
* https://docs.djangoproject.com/en/5.0/topics/testing/overview/ - how to write tests
* https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment
* https://www.digitalocean.com/community/tutorials/django-server-comparison-the-development-server-mod_wsgi-uwsgi-and-gunicorn
* https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/uwsgi/
* https://uwsgi.readthedocs.io/en/latest/tutorials/Django_and_nginx.html
