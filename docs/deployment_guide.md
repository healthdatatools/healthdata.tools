
# Guide to Creating a New Custom Deployment for a Specific Branch

## Prerequisites
- **Access to the production server**
- **Ability to switch users** (`root` and `www-data`)

## Steps for Creating a New Deployment

### 1. Create a New Branch
Create a new branch, e.g., from `main`:
```bash
git checkout -b <yourbranchname> main
```

### 2. Copy the Codebase
Copy the `main` codebase to create a new directory for your branch:
```bash
sudo cp -r /var/www/dev/main /var/www/dev/<yourbranchname>
```

You may have to change owership of this directory structure to the www-user.

### 3. Update the Nginx Configuration
1. Open the Nginx configuration file: `/etc/nginx/sites-enabled/healthdata.tools`
2. Copy and modify the blocks related to `main`:
   - Find the `upstream dev_main_django` block and duplicate it for your branch.
   - Duplicate the associated `server` block.
3. In the new server block, replace the `listen` lines at the bottom with:
   ```nginx
   listen 80;
   listen [::]:80;
   ```

4. Run Certbot for SSL Configuration
   Run Certbot to update SSL certificates, listing all relevant subdomains as the root user:
   ```bash
   certbot --nginx -d healthdata.tools -d main.dev.healthdata.tools -d <...all other existing deployments> <yourname>.dev.healthdata.tools
   ```
   Make sure you run this as the root user.

### 5. Create a New Systemd Service File
Copy and modify the existing `hdt-main.service` file:
```bash
cp /etc/systemd/system/hdt-main.service /etc/systemd/system/hdt-<yourname>.service
```
Edit the new service file to replace `main` with `<yourname>` as needed.

You should also add this file under `config/<yourbranchname>/hdt-<yourname>.service` in the repo for source control purposes. 

### 6. Create Environment Files

Create a new environment file under `/config/<yourname>.env` in your branch on the repo. 

Create another environment file for your branch under `/var/www/dev/<yourbranchname>/secrets/<yourbranchname>.env`

Ensure references to `main` are updated to `<yourname>`.
Also ensure email addresses in environment files reflect the new branch origin.

### 8. Update the Crontab
Edit the crontab for `www-data`:
```bash
crontab -u www-data -e
```
Add a line for your branch similar to the existing `main` entry. Make sure you run the above
command as the www user.

### 9. Create a New `uwsgi.ini` File
Create a new `uwsgi.ini` file at `/config/<yourname>/uwsgi.ini` on your branch in the repo, 
replacing `main` with `<yourname>` as needed.

### 10. Modify the `91-sudoers-www-data-service-restart` File
Edit this file to include your new branch: `/etc/sudoers.d/91-sudoers-www-data-service-restart`

You should also edit this file under `config/91-sudoers-www-data-service-restart` in the repo for source control purposes. 

### 11. Modify `settings.py`
Add your new host to `ALLOWED_HOSTS` in `healthdatatools/settings.py` in the repo

### 12. Update the `deploy.sh` Script
Modify `bin/deploy.sh` to include a conditional deployment for your branch.

### 12. Manually Sync Repo Changes
In the previous step some files were created in the new branch on the repo. Specifically `/config/<yourname>.env`, `/config/<yourname>/uwsgi.ini`, `healthdatatools/settings.py`, and `bin/deploy.sh`. Automated deployments require these to exist properly on the server's version of the filesystem, but they won't automatically get pulled until automatic deployments work (a real chicken and the egg problem). You can manually update the server's versions of these files under
`/var/www/dev/<yourbranchname>/current` to start the service for the first time. 

### 13. Restart the Service
Restart the service using:
```bash
sudo systemctl restart hdt-<yourbranchname>
```

You can may also have to trigger and empty commit on the new brach to kick things off. 

## Useful Commands

Here are some commands that may be useful as you work though the above steps. 

### Change Between Users
```bash
sudo -H -s -u root
sudo -H -s -u www-data
```

### Check Service Status
```bash
systemctl status hdt-<yourname>
```

### Review Logs
```bash
tail -f /tmp/log/<yourname>.hdt.log
tail -f /tmp/log/nginx/{access,error}.log
journalctl -fxeu hdt-<yourname>.service
```

### Ensure Correct Permissions
Ensure the branch directory is owned by `www-data`:
```bash
chown -R www-data:www-data /var/www/dev/<yourbranchname>
```

### Trigger a Redeploy
Create an empty commit and push to trigger a redeploy:
```bash
git commit --allow-empty -m "Trigger redeploy for <yourbranchname>"
git push origin <yourbranchname>
```
