# Chart Library Evaluation and Selection Document

## 1. Goals and Problem Space
The goal of this analysis is to identify a suitable JS charting library that visualizes complex data enabling users to visualize their health data in a clear, interactive, and scalable way. 

> #### Problem Space
> - Diverse Data: Health data comes in various forms, such as time-series and categorical data, with differing formats depending on the source (glucose monitors, heart rate sensors).
> - Scalability: Data can be collected over extended periods, potentially leading to millions of data points that need to be managed.
> - Performance: Charts must load and display data quickly, providing smooth, responsive interactions for users.
> - Accessibility: Visualizations should be easy to interpret and interact with, offering customization options.
> - Compatibility: Charting libraries must integrate with the existing technologies and frameworks, such as React and Django.
> - Cost: The libraries should be affordable or free, easy to learn and use, and have minimal dependencies.

---

## 2. Requirements in Scope
- Show large datasets (10 years * 1 data point every 5 minutes => ~1 million data points)
- Show multiple datasets on the same graph, possibly with different Y-axis scales
- Data can be manipulated interactively client-side
- Data can be formatted for screen, saved as an image, or printed
- Works with the python/django and javascript/react
- Responsive
- Graphs are easy to interpret and interact with
- Low cost or free to use
- Straightforward/easy to learn and use
- Simple dependencies
- Fast and space efficient


---

## 3. Requirements out of Scope
- Doesn't need to include functions for actually calculating averages, smoothing, heatmaps, etc. 
- Doesn't need to include data persistence
- Doesn't need 3D or non-health data compatible graphs
- Doesn't need advanced animations or visualizations
- Doesn't need server-side rendering or processing (should be done client side)

---

## 4. Decision Matrix

| **Feature** | **ChartJS** | **D3JS** | **Recharts** | **HighCharts** | **ApexCharts** |
|---------------------------------|--------------|--------------|--------------|-|-|
| Cost                            | Free | Free | Free | $332/Dev/yr | Free |
| Supports Large Data Sets        | Sorta | Best | Okay | Great | Good |
| Supports Multiple Data Sets     | Limited | Yes | Yes | Yes | Yes |
| Supports Saving                 | Only Images | Manual Config | No | Yes | Yes | 
| Interactive                     | Limited | Yes | May be Limited | Yes | Yes |
| Works with React/Django         | React | React | React | React | React/Django |
| Client-side?                    | Yes | Yes | Yes | Yes | Yes |
| Ease of Use/Learning            | Simple | Low-Level and Complex | Simple | Okay | Easier |
| Size                            | Small | Large | Large | Medium | Small |


---

## 5. Choice and Justification

### 1st Choice: **ApexCharts**
ApexCharts has the best mix of characteristics that match the requirements. It can handle large datasets, interactivity, and export features come standard.

### 2nd Choice: **HighCharts**
HighCharts ticks the same boxes as ApexCharts if not better, however its also a cost prohibitive library targeted towards larger organizations with many features that we wont be needing. 

## 6. Referenced Libraries

ChartJS: https://www.chartjs.org/

D3JS: https://d3js.org/

Recharts: https://recharts.org/en-US

HighCharts: https://www.highcharts.com/

ApexCharts: https://apexcharts.com/