import js from "@eslint/js";
import prettier from "eslint-config-prettier"; // This disables conflicting rules
import babelParser from "@babel/eslint-parser";
import reactPlugin from "eslint-plugin-react";
import reactHooksPlugin from "eslint-plugin-react-hooks";
import globals from "globals";

export default [
  {
    files: [
      "website/**/*.js", 
      "react_components/**/*.{js,jsx}"
    ],
    ignores: [
      "website/static/website/react_output/**/*.js"
    ],
    languageOptions: {
      parser: babelParser, // Use babel parser for JSX
      parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
        ecmaFeatures: {
          jsx: true, // Enable JSX syntax
        },
        requireConfigFile: false,
        babelOptions: {
          presets: ["@babel/preset-react"]
        },
      },
      globals: {
        ...globals.browser,
        ApexCharts: "readonly",
      },
    },
    plugins: {
      react: reactPlugin,
      "react-hooks": reactHooksPlugin,
    },
    rules: {
      ...js.configs.recommended.rules, // ESLint recommended rules
      ...prettier.rules, // Prettier's recommended settings
      ...reactPlugin.configs.recommended.rules, // React Rules
      ...reactHooksPlugin.configs.recommended.rules
    },
    settings: {
      react: {
        version: "detect", // Automatically detects the React version
      },
    },
  },
];
