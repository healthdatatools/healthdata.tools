# healthdata.tools Style Guide

## 1. **Typography**

### Font Family
**Primary font**: <span style="font-family: Verdana, sans-serif">Verdana - sans-serif</span>


### Font Sizes

Text values are set in styles.css at the html level, they can be overridden by any css with more specificity (classes, id, inline, ect).

| Element           | Font Size      |
|-------------------|----------------|
| Heading 1 (H1)    | 2.25 rem (36px)|
| Heading 2 (H2)    | 1.75 rem (28px)|
| Heading 3 (H3)    | 1.5 rem (24px) |
| Large Text        | 1.25 rem (20px)|
| Body Text (p)     | 1 rem (16px)   |
 
### Font Weights
- Regular: 400
- Bold: 700

### Line Height
- Standard line height: 1.75 rem

## 2. **Color Palette**

| Color Name       | Hex Code | Uses |
|------------------|----------|----------------|
|<span style="color: #383F51">Charcoal</span>|#383F51| Dark text, Nav elements|
|<span style="color: #DDDBF1">Lavender</span>|#DDDBF1| Light text, Buttons|
|<span style="color: #3C4F76">Azul</span>|#3C4F76| Buttons, Decoration|
|<span style="color: #D1BEB0">Beige</span>|#D1BEB0|Backgrounds, Decoration|
|<span style="color: #AB9F9D">Quartz</span>|#AB9F9D| Backgrounds, Decoration|

## 3. **Components**

### Button Styles
| Type         | Background Color | Text Color | Border Radius | Padding   |
|--------------|------------------|------------|---------------|-----------|
| Primary      |    Blue(#3C4F76) |   White    |  .375rem(rounded-md)       |   .75rem(p-3)        |
| Secondary    |   Lavender(#DDDBF1)|    Black/Charcoal  |      .375rem(rounded-md)       |   .75rem(p-3)    |

- **Hover Effect**: Slightly lighten the background color on hover for all buttons.

### Forms

#### Input Fields
tbd
#### Form Validation
- Error messages

## 4. **Media Queries**
Element first breakpoints? https://cheewebdevelopment.com/element-first-scss-media-queries/ 
