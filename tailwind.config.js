/** @type {import('tailwindcss').Config} */
import { black as _black, white as _white, gray as _gray, blue as _blue, emerald as _emerald, indigo as _indigo, yellow as _yellow } from 'tailwindcss/colors'
export const content = [
  './templates/**/*.html', // Django templates
  './website/static/**/*.css',
  // './src/**/*.{js,jsx,ts,tsx}', // React components
]
export const theme = {
  extend: {},
  colors: {
    transparent: 'transparent',
    current: 'currentColor',
    black: _black,
    white: _white,
    gray: _gray,
    blue: _blue,
    emerald: _emerald,
    indigo: _indigo,
    yellow: _yellow,
    'charcoal': '#383F51',
    'lavender': '#DDDBF1',
    'azul': '#3C4F76',
    'beige': '#D1BEB0',
    'quartz': '#AB9F9D',
    'white': '#ffffff',
  },
}
export const plugins = []
