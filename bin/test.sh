#!/usr/bin/env bash

set -euxo pipefail

# Linting
echo "Running All Lint Checks..."
npm run lint-ci

# Unit Tests
echo "Running tests..."
poetry run coverage run -m pytest

# Coverage report
echo "Generating coverage report..."
poetry run coverage report --fail-under=80
poetry run coverage html
