#!/usr/bin/env bash

set -euxo pipefail

# This is the entrypoint for building
echo "Executing healthdata.tools build..."

# Check for required commands just to be safe
if ! command -v npm ; then
    echo "Unable to find executable 'npm'"
    exit 1
fi

if ! command -v poetry ; then
    echo "Unable to find executable 'poetry'"
    exit 1
fi

npm install --no-progress
npm run build
poetry install --no-interaction

echo "Build script completed successfully."

