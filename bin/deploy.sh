#!/usr/bin/env bash

# This script is run by the www-data user on the production box to fetch the newest version of the application
echo ""
echo ""
echo ""
echo ""
echo "----------------------------------------------------------------"
echo ""
echo "Starting new deploy"
echo ""
echo "----------------------------------------------------------------"

set -e
set -x

# ensure our path is set correctly
source "$HOME/.bashrc"

# ensure pager is not used
export SYSTEMD_PAGER=

# test some stuff, so we have a good error message if anything is screwed up
if ! command -v git ; then
    echo "Unable to find executable 'git'"
    exit 1
fi
if ! command -v rsync ; then
    echo "Unable to find executable 'rsync'"
    exit 1
fi
if ! command -v poetry ; then
    echo "Unable to find executable 'poetry'"
    exit 1
fi
if ! command -v npm ; then
    echo "Unable to find executable 'npm'"
    exit 1
fi
if ! command -v ssh-agent ; then
    echo "Unable to find executable 'ssh-agent'"
    exit 1
fi
if ! command -v /usr/sbin/service ; then
    echo "Unable to find executable '/usr/sbin/service'"
    exit 1
fi

KEEP_VERSIONS=10
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

GIT_DIR_PATH="$BASE_DIR/.git"

# figure out if we are prod or staging
if [[ "$BASE_DIR" =~ dev/main ]]; then
    DEPLOY_BRANCH="main"
    DEPLOY_SERVICE_NAME="hdt-main"
elif [[ "$BASE_DIR" =~ dev/acampos ]]; then
    DEPLOY_BRANCH="acampos"
    DEPLOY_SERVICE_NAME="hdt-acampos"
elif [[ "$GIT_DIR_PATH" =~ healthdata.tools ]]; then
    DEPLOY_BRANCH="prod"
    DEPLOY_SERVICE_NAME="hdt-prod"
else
    echo "Unrecognized path: '$GIT_DIR_PATH' must contain staging or production, or an alpha environment" 1>&2
    exit 1
fi

FORCE_DEPLOY="${FORCE_DEPLOY:-false}"

ssh-agent bash -c "ssh-add ~/.ssh/id_rsa; git --git-dir='$GIT_DIR_PATH' fetch -t -q origin '$DEPLOY_BRANCH'"

# first figure out if the git checkout has changed
NEW_REV="$(git --git-dir="$GIT_DIR_PATH" rev-parse origin/$DEPLOY_BRANCH)"
WD_REV="$(git --git-dir="$GIT_DIR_PATH" rev-parse HEAD)"
if [[ "$NEW_REV" != "$WD_REV" && "$RE_EXEC" != "true" ]]; then
    cd "$BASE_DIR" && git --git-dir="$GIT_DIR_PATH" reset --hard "origin/$DEPLOY_BRANCH"
    # re-exec ourself, but prevent infinite loop just in case
    echo "Updating repo and Re-executing ourself to run newest version of deploy script..."
    cd "$BASE_DIR" && git --git-dir="$GIT_DIR_PATH" reset --hard "origin/$DEPLOY_BRANCH"
    export RE_EXEC="true"
    export FORCE_DEPLOY="true"
    exec "$BASE_DIR/bin/deploy.sh"
fi

# calculate last successfully deployed rev
CURRENT_REV="$(cat "$BASE_DIR/../current/version.txt")"

echo "Currently Deployed Revision: $CURRENT_REV"
echo "Current Prod Branch Revision: $NEW_REV"

if [[ "$FORCE_DEPLOY" != "true" ]]; then
    if [[ "$CURRENT_REV" == "$NEW_REV" ]]; then
        echo "Nothing to deploy"
        exit 0
    fi
else
    echo "Forcing deploy..."
fi

echo "Setting up new revision $NEW_REV..."

if [[ "$NEW_REV" == "$CURRENT_REV" ]]; then
    if [[ -d "$BASE_DIR/../versions/$NEW_REV" ]]; then
        echo "New rev and current rev are the same, moving current version out of the way"
        mv "$BASE_DIR/../versions/$NEW_REV" "$BASE_DIR/../versions/$NEW_REV.old"
    fi
fi

echo "Updating repo..."
cd "$BASE_DIR" && git --git-dir="$GIT_DIR_PATH" reset --hard "origin/$DEPLOY_BRANCH"

CUR_DIR="$BASE_DIR/../versions/$NEW_REV"

echo "Making directory structure under $CUR_DIR"
mkdir -p "$CUR_DIR"

echo "Copying current version"
rsync -aH --exclude=".git" --exclude=".venv" "$BASE_DIR"/* "$CUR_DIR"

# Build step
echo "-----------------------Changing Directories----------------------------"
cd "$CUR_DIR"
echo "Current Directory: $CUR_DIR"
echo "Contents before build:"
ls -ah "$CUR_DIR"
echo "-----------------------Running BUILD Process ----------------------------"

echo "Running Build"
./bin/build.sh

echo "Collecting static assets..."
poetry run python manage.py collectstatic

echo "Running migrations..."
(
    set -a
    source "config/$DEPLOY_BRANCH/$DEPLOY_BRANCH.env"
    source "$BASE_DIR/../secrets/$DEPLOY_BRANCH.env"
    poetry run python manage.py migrate
)

# we need uwsgi in prod deployments
echo "Installing UWSGI..."
poetry add uwsgi

echo "---------------------END BUILD STEP ----------------------"

# update "current" symlink as an atomic flip
echo "Flipping 'current' symlink from $(readlink "$BASE_DIR/../current") to $BASE_DIR/../versions/$NEW_REV"
# -T -f will ensure existing symlink is overwritten
ln -T -f -s "$BASE_DIR/../versions/$NEW_REV" "$BASE_DIR/../current"

# confirm success by updating version.txt
echo "Writing version.txt stamp file"
echo -n "$NEW_REV" > "$BASE_DIR/../current/version.txt"
echo "Writing domain_version.txt file"
git --git-dir="$GIT_DIR_PATH" describe --long --abbrev=12 --tags > "$BASE_DIR/../current/domain_version.txt"

echo "Restarting service..."
sudo /usr/sbin/service $DEPLOY_SERVICE_NAME restart

echo "Service status:"
sudo /usr/sbin/service $DEPLOY_SERVICE_NAME status

echo "Checking for versions needing cleanup..."
# https://superuser.com/a/708232
VERSIONS_TO_DELETE="$(find "$BASE_DIR/../versions" -maxdepth 1 -type d -printf '%Ts\t%P\n' | sort -n | head -n -"$KEEP_VERSIONS" | cut -f 2-)"
if [[ -n "$VERSIONS_TO_DELETE" ]]; then
    echo "Versions to delete: $VERSIONS_TO_DELETE"
    for i in $VERSIONS_TO_DELETE; do
        echo "Deleting $BASE_DIR/../versions/$i"
        rm -rf "$BASE_DIR/../versions/$i"
    done
fi
echo "Done."
