const path = require('path');
const glob = require('glob');

module.exports = {
  entry: glob.sync('./react_components/entry/**/*.{js,jsx}').reduce((entries, file) => {
    const name = path.basename(file, path.extname(file));  // Get the filename without extension
    
    // Ensure the file path is relative to the current directory, adding './' if necessary
    const relativeFilePath = path.relative(path.resolve(__dirname), file); 
    entries[name] = './' + relativeFilePath;  // Prepend './' to maintain the relative path for Webpack

    return entries;
  }, {}),
  output: {
    path: path.resolve(__dirname, 'website/static/website/react_output'),  // Output directory for bundled files
    filename: '[name].bundle.js',  // Name the output files after the entry point
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,  // Match .js, .jsx, .ts, .tsx files
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',  // Transpile modern JavaScript to browser-compatible JS
              '@babel/preset-react', // Transpile JSX to JavaScript
            ],
          },
        },
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],  // Resolve .js, .jsx, .ts, and .tsx files
  },
};

